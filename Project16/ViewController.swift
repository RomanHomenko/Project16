//
//  ViewController.swift
//  Project16
//
//  Created by Роман Хоменко on 23.06.2022.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let london = Capital(title: "London",
                            coordinate: CLLocationCoordinate2D(
                                latitude: 51.507222,
                                longitude: -0.1275),
                            info: "Home to the 2012 Summer Olympic.")
        let oslo = Capital(title: "Oslo",
                           coordinate: CLLocationCoordinate2D(
                                latitude: 59.95,
                                longitude: 10.75),
                           info: "Founded over a thousand years ago.")
        let paris = Capital(title: "Paris",
                            coordinate: CLLocationCoordinate2D(
                                latitude: 48.8567,
                                longitude: 2.3508),
                            info: "Often called th City of Light.")
        let rome = Capital(title: "Rome",
                            coordinate: CLLocationCoordinate2D(
                                latitude: 41.9,
                                longitude: 12.5),
                            info: "Has a whole country inside of it.")
        let washington = Capital(title: "Washington DC",
                            coordinate: CLLocationCoordinate2D(
                                latitude: 38.895111,
                                longitude: -77.036667),
                            info: "Named after George himself.")
        
        mapView.addAnnotations([london, oslo, paris, rome, washington])
        
        let mapTypeBtn: UIButton = {
            let btn = UIButton(type: .system)
            
            btn.backgroundColor = .blue
            btn.setTitle("Change map Type", for: .normal)
            btn.setTitleColor(.white, for: .normal)
            btn.layer.cornerRadius = CGFloat(10)
            btn.addTarget(self, action: #selector(showMapTypesAction), for: .touchUpInside)
            
            return btn
        }()
        
        
        self.view.addSubview(mapTypeBtn)
        
        mapTypeBtn.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([mapTypeBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     mapTypeBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30),
                                     mapTypeBtn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 50),
                                     mapTypeBtn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -50),
                                     mapTypeBtn.heightAnchor.constraint(equalToConstant: 60)])
    }
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is Capital else { return nil }
        
        let identifier = "Capital"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            annotationView?.tintColor = .green
            
            let btn = UIButton(type: .detailDisclosure)
            annotationView?.rightCalloutAccessoryView = btn
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "webview") as? WikipediaViewController else { return }
        vc.city = view.annotation?.title! ?? "russia"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - Selectors for buttons
extension ViewController {
    @objc func showMapTypesAction() {
        let alert = UIAlertController(title: "Map types",
                                      message: "chose one",
                                      preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "sattellite", style: .default) { [weak mapView] _ in
            mapView!.mapType = .satellite
        })
        alert.addAction(UIAlertAction(title: "mutedStandard", style: .default) { [weak mapView] _ in
            mapView!.mapType = .mutedStandard
        })
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
    }
}
