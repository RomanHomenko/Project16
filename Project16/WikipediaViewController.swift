//
//  WikipediaViewController.swift
//  Project16
//
//  Created by Роман Хоменко on 23.06.2022.
//

import UIKit
import WebKit

class WikipediaViewController: UIViewController, WKUIDelegate {

    @IBOutlet weak var webView: WKWebView!
    
    // Properites
    var city: String = "" {
        didSet {
            city = "/\(city)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let url = URL(string: "https://ru.wikipedia.org/wiki" + String(describing: city)) else {
            return
        }
        let request = URLRequest(url: url)
        webView.load(request)
    }
}
